Dependencies
------------

make gcc cmake

pkg-config build-essential


Distribution
------------

npm install --save-dev @nodegui/packer

npx nodegui-packer --init nodegui-boilerplate

npx nodegui-packer --pack dist
