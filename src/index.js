const { QMainWindow, QWidget } = require('@nodegui/nodegui');

const win = new QMainWindow();
win.setWindowTitle('nodegui boilerplate');
win.resize(400, 200);

// Root view
const rootView = new QWidget();

win.setCentralWidget(rootView);
win.show();

global.win = win;
